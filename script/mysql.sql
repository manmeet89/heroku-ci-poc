CREATE  TABLE users (
  username VARCHAR(45) NOT NULL ,
  password VARCHAR(45) NOT NULL ,
  enabled SMALLINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (username));

CREATE SEQUENCE user_role_id_seq;

  CREATE TABLE user_roles (
  user_role_id INTEGER NOT NULL DEFAULT nextval('user_role_id_seq'),
  username varchar(45) NOT NULL,
  role varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  CONSTRAINT fk_username FOREIGN KEY (username) REFERENCES users (username));
  



INSERT INTO users(username,password,enabled)
VALUES ('manmeet','password', 1);
INSERT INTO users(username,password,enabled)
VALUES ('admin','fidelity', 1);

INSERT INTO user_roles (username, role)
VALUES ('manmeet', 'ROLE_USER');
INSERT INTO user_roles (username, role)
VALUES ('admin', 'ROLE_ADMIN');
INSERT INTO user_roles (username, role)
VALUES ('admin', 'ROLE_USER');  