# ReadMe Documentation
----------------------------------------------------------------------

- **Project** : Identity Provider (IDP)
- **Owner** : CRM
- **Author** : Manmeet Singh
- **Date** : 10/12/2015
----------------------------------------------------------------------
# Application Description
Identity provider is an application which is developed for the CRM eco-system to manage authentication of communication between various applications.

It is the central application which is responsible to authenticate the request between any service provider and service client.

It is a Spring Boot based application.

# Installation

#### Prerequisites
The list of softwares/ applications required to run the project on local machine

- Git
- Eclipse
- Maven
- JDK 1.8 or above
- Postgress client

#### Initial Setup
Run the DB scripts committed under scripts folder in the following order

1. CreateTable.sql
2. Constraints.sql
3. inserts.sql

The IDP application uses **RSA asymmetric crytography** algorithm which requires a pair of Public and Private keys. These keys are maintained in the **SYS_PARAM** relations.

If in future their is a requirement of changing these keys then update in the table **SYS_PARAM**

## Heroku Deployment
The application is developed to be available on public PAAS **Heroku**. The commands to be run in the order are below :

#### First Time deployment
```sh
git config user.email 'your emailid configurred in heroku'
git init
git add .
git commit -m "Initial Commit"
heroku login		
	
--Create the Heroku application							
heroku create 'crm-idp-prod'

--Add the postgres DB addon to the application just created
heroku addons:create heroku-postgresql:standard-0

--List all the environment variables
heroku config -a crm-idp-prod

--Promote the newly added DB as primary DB.
heroku pg:promote 'HEROKU_POSTGRESQL_ORANGE_URL' -a 'crm-idp-prod'

--Set LOG_LEVEL config var 
heroku config:set LOG_LEVEL="INFO"
git push heroku master
```

#### Subsequent deployments
```sh
cd 'Location where project is cloned'
git config user.email 'your emailid configurred in heroku'
git init
git add .
git commit -m "Commit comments"
heroku login		
heroku git:remote -a 'crm-idp-prod'
git push heroku master
```

