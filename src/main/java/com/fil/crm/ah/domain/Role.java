/**
 *
 */
package com.fil.crm.ah.domain;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author ManmeetFIL
 *
 */
public class Role implements GrantedAuthority {

	private String name;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.core.GrantedAuthority#getAuthority()
	 */
	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
