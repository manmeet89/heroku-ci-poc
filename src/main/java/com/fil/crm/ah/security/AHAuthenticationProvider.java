/**
 *
 */
package com.fil.crm.ah.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.fil.crm.ah.dao.SecurityDAO;
import com.fil.crm.ah.domain.User;

/**
 * @author ManmeetFIL
 *
 */
public class AHAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	SecurityDAO securityDAO;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * authenticate(org.springframework.security.core.Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		final String loginUsername = (String) authentication.getPrincipal();
		final String loginPassword = (String) authentication.getCredentials();
		User user = securityDAO.getUser(loginUsername, loginPassword);
		if (user == null) {
			throw new BadCredentialsException("Invalid username or password");
		}
		user.setAuthorities(securityDAO.getRoles(authentication.getPrincipal().toString()));
		System.out.println("User ::" + user);
		return new UsernamePasswordAuthenticationToken(user, loginPassword, user.getAuthorities());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.authentication.AuthenticationProvider#
	 * supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
}
