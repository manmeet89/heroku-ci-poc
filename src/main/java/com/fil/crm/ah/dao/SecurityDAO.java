/**
 *
 */
package com.fil.crm.ah.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.fil.crm.ah.domain.Role;
import com.fil.crm.ah.domain.User;

/**
 * @author ManmeetFIL
 *
 */
@Repository
public class SecurityDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public User getUser(final String userName, final String password) {
		try {
			return jdbcTemplate.query("SELECT * FROM USERS U WHERE U.USERNAME = ? AND U.PASSWORD = ?",
					new Object[] { userName, password }, (ResultSetExtractor<User>) rs -> {
						if (!rs.next()) {
							return null;
						}
						User user = new User();
						user.setUserName(rs.getString("USERNAME"));
						user.setPassword(rs.getString("PASSWORD"));
						user.setEnabled(rs.getBoolean("enabled"));
						return user;
					});
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	public List<Role> getRoles(final String userName) {
		try {
			return jdbcTemplate.query("SELECT * FROM USER_ROLES WHERE USERNAME = ?", new Object[] { userName },
					(RowMapper<Role>) (rs, rowNum) -> {
						Role role = new Role();
						role.setName(rs.getString("ROLE"));
						return role;
					});
		} catch (DataAccessException exception) {
			exception.printStackTrace();
		}
		return null;
	}
}
