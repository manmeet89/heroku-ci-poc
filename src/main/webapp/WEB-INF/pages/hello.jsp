<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<title>Welcome CRM</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"	href="${pageContext.request.contextPath}/css/bootstrap.min.css">
<script	src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script	src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

<style type="text/css">
.tab-content {
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    height: 100%;
    padding: 10px;
}
</style>
<script type="text/javascript">
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
			$(document).ready(function(){
				$('.tab').click(function() {
					$('.tab').removeClass('active');
					$(this).addClass('active');
					$('.tab-content').hide();
					$('#' + $(this).attr('id') + '-content').show();
				});
			});
		</script>
</head>
<body>

	<sec:authorize access="hasRole('ROLE_USER')">
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
		<form action="${logoutUrl}" method="post" id="logoutForm">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<div style="float: right;padding: 20px">
				<h3>
					<b>${pageContext.request.userPrincipal.name}</b> | <a
						href="javascript:formSubmit()"> Logout</a>
				</h3>
			</div>
		</c:if>


		<div class="container">
			<h3>Functionalities</h3>
			<div class="tabs">
				<ul class="nav nav-tabs">
					<li class="tab active" id="tab1"><a href="#">Generic Page</a></li>
					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<li class = "tab" id="tab2"><a href="#">Manage Users</a></li>
						<li class = "tab" id="tab3"><a href="#">Manage System Parameters</a></li>
					</sec:authorize>
				</ul>
				<div class = "tab-content" id="tab1-content">
					Content tab 1
				</div>
				
				<div class = "tab-content" id="tab2-content" style="display: none">
					Content tab 2
				</div>
				
				<div class = "tab-content" id="tab3-content" style="display: none">
					Content tab 3
				</div>
				
			</div>
		</div>
	</sec:authorize>

</body>
</html>
