<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page session="true"%>
<html>
<head>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
<link rel="stylesheet"	href="${pageContext.request.contextPath}/css/bootstrap.min.css">
<script	src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script	src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
  
<title>Login Page</title>
<style>
.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

#login-box {
	width: 300px;
	padding: 20px;
	margin: 100px auto;
	background: #fff;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
}
</style>
</head>
<body onload='document.loginForm.username.focus();'>

	
	
	<div class="page-header">
    <h1>CRM Audit History</h1>
  </div>

	<div id="login-box">

		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>

		<%-- <form name='loginForm'
			action="<c:url value='/j_spring_security_check' />" method='POST'>

			<table>
				<tr>
					<td>User:</td>
					<td><input type='text' name='username'></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type='password' name='password' /></td>
				</tr>
				<tr>
					<td colspan='2'><input name="submit" type="submit"
						value="submit" /></td>
				</tr>
			</table>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

		</form> --%>
		
		<form name='loginForm'
			action="<c:url value='/j_spring_security_check' />" method='POST'>
    <h3>Log In</h3> 
    <div class="form-group">
        <input name="username" type="text" class="form-control input-lg" placeholder="Username" />
    </div>
    <div class="form-group">
        <input name="password" type="password" class="form-control input-lg" placeholder="Password"/>
    </div>
    <div class="form-group">
    <button class="btn btn-primary btn-lg btn-block loginBtn"
        style="font-size: 20px; font-family: verdana; background-color: #007AFF" name="submit" type="submit">
        Submit</button>
    </div>
    <input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
	</form>
	</div>

</body>
</html>